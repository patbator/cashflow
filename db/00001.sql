CREATE TABLE IF NOT EXISTS "subject" (id INTEGER PRIMARY KEY, label TEXT, description TEXT);
CREATE TABLE IF NOT EXISTS "tag" (id INTEGER PRIMARY KEY, label TEXT);
CREATE TABLE IF NOT EXISTS "subject_tag" (id INTEGER PRIMARY KEY, subject_id INTEGER, tag_id INTEGER);
CREATE TABLE IF NOT EXISTS "graph_tag" (id INTEGER PRIMARY KEY, graph_id INTEGER, tag_id INTEGER);
CREATE TABLE IF NOT EXISTS "graph" (id INTEGER PRIMARY KEY, label TEXT not null, start TEXT null, end TEXT null, types TEXT, directions TEXT, settings TEXT);
CREATE TABLE IF NOT EXISTS "flow" (
	"id"	INTEGER NOT NULL,
	"value_date"	TEXT NOT NULL,
	"type"	TEXT NOT NULL,
	"in"	REAL NOT NULL DEFAULT 0.0,
	"out"	REAL NOT NULL DEFAULT 0.0,
	"details"	TEXT NOT NULL DEFAULT '',
	"raw"	TEXT NOT NULL DEFAULT '',
        "source_id" TEXT NOT NULL DEFAULT '',
	"subject_id"	INTEGER NOT NULL,
	PRIMARY KEY("id")
);
CREATE INDEX subject_label on subject(label);
CREATE INDEX tag_label on tag(label);
CREATE UNIQUE INDEX subject_tag_uniq on subject_tag(subject_id, tag_id);
CREATE UNIQUE INDEX graph_tag_uniq on graph_tag(graph_id, tag_id);
CREATE INDEX flow_subject_id ON flow (subject_id);
CREATE INDEX flow_type ON flow (type);
CREATE INDEX flow_value_date ON flow (value_date);
CREATE INDEX flow_source_id on flow (source_id);
