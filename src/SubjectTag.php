<?php
namespace Cashflow;

use \Storm\Model\ModelAbstract;


class SubjectTag extends ModelAbstract
{
    protected
        $_table_name = 'subject_tag',
        $_belongs_to = [
            'subject' => ['model' => Subject::class],
            'tag' => ['model' => Tag::class],
        ];
}
