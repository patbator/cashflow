<?php
namespace Cashflow\Action;

use \Cashflow\Flow;
use \Cashflow\Tag;
use \Cashflow\Graph;


class Index extends Base
{
    public function __invoke($request, $response, $args)
    {
        return $this->_respondOn(
            $response,
            [
                'months'         => Flow::findAllGroupedByMonth(),
                'graphs'         => Graph::findAll(),
                'month_mean'     => Flow::ofLastMonth()->getBalance(),
                'quarter_mean'   => Flow::ofLastQuarter()->getBalance(),
                'semester_mean'  => Flow::ofLastSemester()->getBalance(),
                'ninemester_mean'=> Flow::ofLastNinemester()->getBalance(),
                'year_mean'      => Flow::ofLastYear()->getBalance()
            ]
        );
    }
}
