<?php
namespace Cashflow\Action;

use \Cashflow\Flow;


class Subject extends Base
{
    public function __invoke($request, $response, $args)
    {
        return $this->_respondOn(
            $response,
            [
                'subject' => $args['subject'],
                'flows' => Flow::ofSubject($args['subject'])
            ]
        );
    }
}
