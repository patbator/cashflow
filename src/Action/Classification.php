<?php
namespace Cashflow\Action;

use \Cashflow\Subject;
use \Storm\Model\Collection;


class Classification extends Base
{
    public function __invoke($request, $response, $args)
    {
        return $this->_respondOn($response,
                                 ['subjects' => new Collection(Subject::findAll())]);
    }
}
