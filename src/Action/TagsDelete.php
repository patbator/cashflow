<?php
namespace Cashflow\Action;

use \Cashflow\Tag;


class TagsDelete extends Base
{
    public function __invoke($request, $response, $args)
    {
        if (!$tag = Tag::find($args['id']))
            return $response->withRedirect($this->_router->pathFor('tags'));

        $tag->delete();
        return $response->withRedirect($this->_router->pathFor('tags'));
    }
}
