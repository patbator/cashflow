<?php
namespace Cashflow\Action;

use \Cashflow\Tag;


class TagsEdit extends Base
{
    public function __invoke($request, $response, $args)
    {
        $tag = isset($args['id'])
            ? Tag::find((int)$args['id'])
            : Tag::newInstance();

        $title = $tag->isNew()
            ? 'Nouveau tag'
            : sprintf('Modifier le tag "%s"', $tag->getLabel());

        if (!$request->isPost())
            return $this->_respondOn($response, ['title' => $title,
                                                 'form' => $tag->form()]);

        $tag->setLabel($request->getParam('label'));
        if (!$tag->isValid())
            return $this->_respondOn(
                $response,
                ['tag' => $tag,
                 'errors' => $tag->getErrors()]
            );

        $tag->save();

        return $response->withRedirect($this->_router->pathFor('tags'));
    }
}
