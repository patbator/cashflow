<?php
namespace Cashflow\Action;

class Base
{
    protected $_view, $_router;

    public function __construct($container)
    {
        $this->_view = $container['renderer'];
        $this->_router = $container['router'];
    }


    public function _respondOn($response, $datas=[], $template=null)
    {
        if (null === $template) {
            $parts = explode('\\', get_class($this));
            $parts = preg_split('/(?=[A-Z])/', end($parts));
            $template = strtolower(implode('-', array_filter($parts))) . '.phtml';
        }

        return $this->_view->render($response, $template,
                                    array_merge(['router' => $this->_router], $datas));
    }
}
