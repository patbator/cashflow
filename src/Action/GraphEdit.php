<?php
namespace Cashflow\Action;

use \Cashflow\Graph;
use \Cashflow\Tag;
use \Storm\Persistence\Connections;


class GraphEdit extends Base
{
    public function __invoke($request, $response, $args)
    {
        $graph = isset($args['id']) ? Graph::find($args['id']) : Graph::newInstance();
        $graph = $graph ? $graph : Graph::newInstance();

        $title = $graph->isNew()
            ? 'Nouveau graphique'
            : ('Modifier le graphique "' . $graph->getLabel()) . '"';

        if (!$request->isPost())
            return $this->_respondOn($response, ['title' => $title,
                                                 'model' => $graph,
                                                 'form' => $graph->form()]);

        $params = $request->getParams();
        $params['tags'] = isset($params['tags']) ? Tag::findAllBy(['id' => $params['tags']]) : [];
        $params['types'] = isset($params['types']) ? $params['types'] : [];
        $params['directions'] = isset($params['directions']) ? $params['directions'] : [];
        $graph->updateAttributes($params);

        if (!$graph->isValid())
            return $this->_respondOn($response, ['title' => $title,
                                                 'model' => $graph,
                                                 'form' => $graph->form()]);

        $graph->save();

        return $response->withRedirect(
            $this->_router->pathFor('graph-edit', ['id' => $graph->getId()])
        );
    }
}
