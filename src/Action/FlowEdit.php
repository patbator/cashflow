<?php
namespace Cashflow\Action;

use \Cashflow\Flow;
use \Cashflow\Subject;


class FlowEdit extends Base
{
    public function __invoke($request, $response, $args)
    {
        if (!$flow = Flow::find($args['id']))
            return $response->withRedirect($this->_router->pathFor('home'));

        if (!$request->isPost())
            return $this->_respondOn($response, [
                'title' => $flow->getTypeLabel() . ' du ' . $flow->getValueDate(),
                'flow'  => $flow,
                'form'  => $flow->form()
            ]);

        if (($subject = $request->getParam('subject'))
            && $flow->getSubjectLabel() != $subject) {
            $flow->setSubject(Subject::findOrCreateByLabel($subject));
        }

        if (($value_date = $request->getParam('value_date'))
            && $flow->getValueDate() != $value_date
            && $value_date === date('Y-m-d', strtotime($value_date))) {
            $flow->setValueDate($value_date);
        }

        $flow->save();

        return $response->withRedirect(
            $this->_router->pathFor('month', ['month' => $flow->getMonth()])
        );
    }
}
