<?php
namespace Cashflow\Action;

use \Cashflow\Graph;


class GraphDelete extends Base
{
    public function __invoke($request, $response, $args)
    {
        if (!$model = Graph::find($args['id']))
            return $response->withRedirect($this->_router->pathFor('home'));

        $model->delete();
        return $response->withRedirect($this->_router->pathFor('home'));
    }
}
