<?php
namespace Cashflow\Action;

use \Cashflow\Subject;
use \Cashflow\Tag;
use \Storm\Model\Collection;


class Classify extends Base
{
    public function __invoke($request, $response, $args)
    {
        $subjects = $this->_subjectsFor($args['set']);
        $subject = null;
        if (array_key_exists('page', $args)
            && ($subject = $subjects[$args['page']])) {
            $page = $args['page'];
        }

        if (!$subject) {
            $subject = $subjects->first();
            $page = 0;
        }

        $next_url = $page == $subjects->count()-1
            ? $this->_router->pathFor('classification')
            : $this->_router->pathFor('classify', ['set' => $args['set'],
                                                   'page' => $page+1]);

        $flows = new Collection($subject->getFlows());
        $flows->uasort(function($a, $b) { return strcmp($a->getValueDate(), $b->getValueDate()); });

        if (!$request->isPost())
            return $this->_respondOn($response, ['subject' => $subject,
                                                 'form' => $subject->form(),
                                                 'current_set' => $args['set'],
                                                 'current' => $page,
                                                 'total' => $subjects->count(),
                                                 'next_url' => $next_url,
                                                 'flows' => $flows]);

        $subject
            ->setDescription($request->getParam('description'))
            ->setTags(Tag::findAllBy(['id' => $request->getParam('tags')]));

        $subject->save();

        return $response->withRedirect($next_url);
    }


    protected function _subjectsFor($set)
    {
        $subjects = new Collection(Subject::findAll());
        if ('all' == $set)
            return $subjects;

        if ('classified' == $set)
            return $subjects->select(function($subject) { return $subject->isClassified(); });

        if ('not-classified' == $set)
            return $subjects->select(function($subject) { return !$subject->isClassified(); });

        if ('type-' === substr($set, 0, 5))
            return $subjects->select(function($subject) use($set) { return $subject->isType(substr($set, 5)); });

        return $subjects;
    }
}
