<?php
namespace Cashflow\Action;

use \Cashflow\Tag;


class TagsIndex extends Base
{
    public function __invoke($request, $response, $args)
    {
        return $this->_respondOn($response, ['tags' => Tag::findAllBy(['order' => 'label'])]);
    }
}
