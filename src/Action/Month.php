<?php
namespace Cashflow\Action;

use \Cashflow\Flow;


class Month extends Base
{
    public function __invoke($request, $response, $args)
    {
        return $this->_respondOn($response, ['month' => $args['month'],
                                             'flows' => Flow::ofMonth($args['month'])]);
    }
}
