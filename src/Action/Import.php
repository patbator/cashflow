<?php
namespace Cashflow\Action;

use \Cashflow\Form;
use \Cashflow\Flow\Import\CaisseEpargneCsv;


class Import extends Base
{
    public function __invoke($request, $response, $args)
    {
        $form = (new Form())
            ->addFile('datas', ['label' => 'Fichier de données',
                                'required' => true,
                                'description' => 'Seul le format csv Caisse d\'Épargne est supporté']);

        if (!$request->isPost())
            return $this->_respondOn($response, ['form' => $form]);

        $files = $request->getUploadedFiles();
        if (!array_key_exists('datas', $files))
            throw new \RuntimeException('Aucun fichier transmis');

        $report = (new CaisseEpargneCsv($files['datas']->file))->run();
        return $this->_respondOn($response, ['form' => $form,
                                             'report' => $report]);
    }
}
