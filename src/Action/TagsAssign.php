<?php
namespace Cashflow\Action;

use \Cashflow\Tag;


class TagsAssign extends Base
{
    public function __invoke($request, $response, $args)
    {
        if ((!$tag = Tag::find($request->getParam('name')))
            || !$subject = $request->getParam('subject'))
            return $response->withJson(['status' => false]);

        $tag->addTarget($subject)
            ->save();

        return $response->withJson(['status' => true]);
    }
}
