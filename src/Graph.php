<?php
namespace Cashflow;

use \Storm\Model\ModelAbstract;
use \Storm\Model\Collection;


class Graph extends ModelAbstract
{
    protected
        $_table_name = 'graph',
        $_default_attribute_values = ['label' => '',
                                      'start' => null,
                                      'end' => null,
                                      'types' => '',
                                      'directions' => '',
                                      'settings' => ''],

        $_has_many = [
            'graphtags' => ['model' => GraphTag::class,
                            'role' => 'graph',
                            'dependents' => 'delete',
                            'unique' => true],
            'tags' => ['through' => 'graphtags',
                       'unique' => true],
        ];


    public function validate()
    {
        $this->checkAttribute('label', '' !== trim($this->getLabel()), 'Le libellé est obligatoire');
        if ($this->hasStart() && $this->hasEnd()
            && (new \DateTime($this->getStart())) >= (new \DateTime($this->getEnd()))) {
            $this->addAttributeError('start', 'La date de début doit être avant la date de fin');
            $this->addAttributeError('end', 'La date de fin doit être après la date de début');
        }
    }


    public function getTagsIds()
    {
        return (new Collection($this->getTags()))->collect('id')->getArrayCopy();
    }


    public function getTypesArray()
    {
        return explode(',', $this->getTypes());
    }


    public function setTypes($string_or_array)
    {
        if (is_array($string_or_array))
            $string_or_array = implode(',', $string_or_array);

        return parent::_set('types', $string_or_array);
    }


    public function getDirectionsArray()
    {
        return explode(',', $this->getDirections());
    }


    public function setDirections($string_or_array)
    {
        if (is_array($string_or_array))
            $string_or_array = implode(',', $string_or_array);

        return parent::_set('directions', $string_or_array);
    }


    public function form()
    {
        $date_range = Flow::dateRange();

        return (new Form)
            ->withErrors($this->getErrors())
            ->addRequiredText('label', ['label' => 'Libellé',
                                        'value' => $this->getLabel()])

            ->addDate('start', ['label' => 'Début',
                                'value' => $this->getStart(),
                                'min' => $date_range->fromAsDate(),
                                'max' => $date_range->toAsDate()])

            ->addDate('end', ['label' => 'Fin',
                              'value' => $this->getEnd(),
                              'min' => $date_range->fromAsDate(),
                              'max' => $date_range->toAsDate()])

            ->addCheckboxes('types', ['label' => 'Types',
                                      'value' => $this->getTypesArray(),
                                      'options' => Flow::knownTypes()])

            ->addCheckboxes('directions', ['label' => 'Directions',
                                           'value' => $this->getDirectionsArray(),
                                           'options' => Flow::knownDirections()])

            ->addCheckboxes('tags', ['label' => 'Tags',
                                     'value' => $this->getTagsIds(),
                                     'options' => Tag::asFormOptions()])

            ;
    }


    public function getFlows()
    {
        return (new Collection(Flow::findAll()))->select(function($flow) {
            if ($this->hasStart()
                && new \DateTime($this->getStart()) > new \DateTime($flow->getValueDate()))
                return false;

            if ($this->hasEnd()
                && new \DateTime($this->getEnd()) < new \DateTime($flow->getValueDate()))
                return false;

            if ($this->hasTypes()
                && !in_array($flow->getType(), $this->getTypesArray()))
                return false;

            if ($this->hasDirections()
                && !in_array($flow->getDirection(), $this->getDirectionsArray()))
                return false;

            if (!$this->hasTags())
                return true;

            if (!$flow_tags = $flow->getTags())
                return false;

            if (!array_intersect((array)(new Collection($flow_tags))->collect('id'),
                                 $this->getTagsIds()))
                return false;

            return true;
        });
    }
}
