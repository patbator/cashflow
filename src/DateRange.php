<?php
namespace Cashflow;

class DateRange
{
    protected $_from, $_to;

    public function __construct($from=null, $to=null)
    {
        $this->_from = $from;
        $this->_to = $to;
    }


    public function setFrom($from)
    {
        $this->_from = $from;
        return $this;
    }


    public function setFromIfBefore($from)
    {
        return (null == $this->_from || $from < $this->_from)
            ? $this->setFrom($from)
            : $this;
    }


    public function setTo($to)
    {
        $this->_to = $to;
        return $this;
    }


    public function setToIfAfter($to)
    {
        return (null == $this->_to || $to > $this->_to)
            ? $this->setTo($to)
            : $this;
    }


    public function fromAsDate()
    {
        return date('Y-m-d', $this->_from);
    }


    public function toAsDate()
    {
        return date('Y-m-d', $this->_to);
    }
}
