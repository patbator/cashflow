<?php

$app->get('/', \Cashflow\Action\Index::class)->setName('home');
$app->get('/classification', \Cashflow\Action\Classification::class)->setName('classification');
$app->map(['GET', 'POST'], '/import', \Cashflow\Action\Import::class)->setName('import');

$app->map(['GET', 'POST'], '/classification/{set}[/{page}]', \Cashflow\Action\Classify::class)->setName('classify');
$app->get('/subject/{subject}', \Cashflow\Action\Subject::class)->setName('subject');
$app->get('/tags', \Cashflow\Action\TagsIndex::class)->setName('tags');
$app->map(['GET', 'POST'], '/tags/edit[/{id}]', \Cashflow\Action\TagsEdit::class)->setName('tag-edit');
$app->get('/tags/assign', \Cashflow\Action\TagsAssign::class)->setName('tag-assign');
$app->get('/tags/unassign', \Cashflow\Action\TagsUnassign::class)->setName('tag-unassign');
$app->get('/tags/delete/{id}', \Cashflow\Action\TagsDelete::class)->setName('tag-delete');
$app->map(['GET', 'POST'], '/flow/edit[/{id}]', \Cashflow\Action\FlowEdit::class)->setName('flow-edit');

$app->map(['GET', 'POST'], '/graph/edit[/{id}]', \Cashflow\Action\GraphEdit::class)->setName('graph-edit');
$app->get('/graph/delete/{id}', \Cashflow\Action\GraphDelete::class)->setName('graph-delete');

$app->get('/{month}', \Cashflow\Action\Month::class)->setName('month');
