<?php
namespace Cashflow\View;


class GraphDataset
{
    protected
        $_type = 'line',
        $_labels_callback,
        $_datasets_callback
        ;


    public function type($type)
    {
        $this->_type = $type;
        return $this;
    }


    public function labelsCallback($callback)
    {
        $this->_labels_callback = $callback;
        return $this;
    }


    public function datasetsCallback($callback)
    {
        $this->_datasets_callback = $callback;
        return $this;
    }


    public function acceptVisitor($visitor)
    {
        $visitor
            ->visitType($this->_type)
            ->visitLabelsCallback($this->_labels_callback)
            ->visitDatasetCallback($this->_datasets_callback);
    }
}
