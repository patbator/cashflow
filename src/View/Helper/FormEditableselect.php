<?php
namespace Cashflow\View\Helper;


class FormEditableselect extends FormSelect
{
    protected function _renderInput()
    {
        $this->_view
            ->addStyleSheet('//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.css')
            ->addHeadScript('//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.js')
            ->addDocumentReady('$("#'. $this->_element->getId()  .'").editableSelect();');

        return parent::_renderInput();
    }
}
