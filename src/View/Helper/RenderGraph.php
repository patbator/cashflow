<?php
namespace Cashflow\View\Helper;


class RenderGraph extends Base
{
    protected
        $_type = 'line',
        $_labels = [],
        $_datasets = [],
        $_items;

    public function __invoke($id, $description, $items)
    {
        $this->_items = $items;
        $description->acceptVisitor($this);

        return
            $this->_tag('canvas', '', ['id' => $id,
                                       'width' => '400',
                                       'height' => '200'])
            . $this->_tag('script', $this->_renderScript($id));
    }


    protected function _renderScript($id)
    {
        $description = [
            'type' => $this->_type,
            'data' => [
                'labels' => $this->_labels,
                'datasets' => $this->_datasets
            ],
            'options' => [
                'scales' => [
                    'yAxes' => [
                        ['ticks' => ['beginAtZero' => true]]
                    ]
                ]
            ]
        ];
        return 'var ctx = document.getElementById("' . $id . '");'
            . 'new Chart(ctx, '. json_encode($description) . ');';
    }


    public function visitType($type)
    {
        $this->_type = $type;
        return $this;
    }


    public function visitLabelsCallback($callback)
    {
        $this->_labels = $callback($this->_items);
        return $this;
    }


    public function visitDatasetCallback($callback)
    {
        $this->_datasets = $callback($this->_items);
        return $this;
    }
}
