<?php
namespace Cashflow\View\Helper;


abstract class Base
{
    protected $_view;

    public function __construct($view)
    {
        $this->_view = $view;
    }


    protected function _tag($name, $content, $attribs=[])
    {
        return call_user_func([$this->_view, 'tag'], $name, $content, $attribs);
    }
}
