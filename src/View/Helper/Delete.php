<?php
namespace Cashflow\View\Helper;


class Delete extends Base
{
    public function __invoke($label, $url)
    {
        return $this->_view->action($label, $url, 'times', 'danger',
                                    ['onclick' => 'return confirm(\''. $label .' ?\');']);
    }
}
