<?php
namespace Cashflow\View\Helper;


class FormCheckboxes extends FormElement
{
    public function __invoke($element)
    {
        $this->_element = $element;

        return $this->_tag(
            'fieldset',
            $this->_renderLabel() . $this->_renderInput() . $this->_renderError(),
            ['class' => 'form-group']
        );
    }


    protected function _renderLabel()
    {
        return $this->_tag('legend', $this->_element->getLabel() . ' ' . $this->_renderAllNone());
    }


    protected function _renderAllNone()
    {
        return $this->_tag('small',
                           $this->_tag('a',
                                       $this->_view->fontAwesome('check-square') . ' Tous',
                                       ['href' => '#',
                                        'onclick' => '$(this).closest(\'fieldset\').find(\'label\').each(function(i, node) { if (!$(node).hasClass(\'active\')) $(node).click(); });return false;'])
                           . ' / '
                           . $this->_tag('a',
                                         $this->_view->fontAwesome('square')
                                         . ' Aucun',
                                         ['href' => '#',
                                          'onclick' => '$(this).closest(\'fieldset\').find(\'label\').each(function(i, node) { if ($(node).hasClass(\'active\')) $(node).click(); });return false;']),
                           ['style' => 'font-size: 65%']);
    }


    protected function _renderInput()
    {
        $options = [];
        foreach($this->_element->getOptions() as $value => $label)
            $options[] = $this->_renderOption($value, $label);

        return implode("\n", $options);
    }


    protected function _renderOption($value, $label)
    {
        $id = $this->_element->getId() . '-' . $value;
        $attribs = ['type' => 'checkbox',
                    'name' => $this->_element->getName() . '[]',
                    'id' => $id,
                    'value' => $value];

        $active = '';
        if (in_array($value, $this->_element->getValue())) {
            $attribs['checked'] = 'checked';
            $active = 'active';
        }

        return $this
            ->_tag('div',
                   $this->_tag('label',
                               $this->_tag('input', null, $attribs) . ' ' . $label,
                               ['class' => 'btn btn-secondary ' . $active, 'for' => $id]),
                   ['class' => 'btn-group btn-group-toggle',
                    'data-toggle' => 'buttons']);
    }
}
