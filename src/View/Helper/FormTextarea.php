<?php
namespace Cashflow\View\Helper;


class FormTextarea extends FormElement
{
    protected function _renderInput()
    {
        $attribs = ['name' => $this->_element->getName(),
                    'id' => $this->_element->getId(),
                    'cols' => $this->_element->getCols(),
                    'rows' => $this->_element->getRows(),
                    'class' => 'form-control'];

        if ($this->_element->isRequired())
            $attribs['required'] = 'required';

        return $this->_tag('textarea', $this->_element->getValue(), $attribs);
    }
}
