<?php
namespace Cashflow\View\Helper;


class FormFile extends FormElement
{
    protected function _inputAttribs()
    {
        return ['type' => 'file'];
    }
}
