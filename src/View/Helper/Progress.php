<?php
namespace Cashflow\View\Helper;


class Progress extends Base
{
    public function __invoke($max, $min, $current)
    {
        $percent = ($current*100) / $max;

        return $this
            ->_tag('div',
                   $this->_tag('div', floor($percent) . ' %',
                               ['class' => 'progress-bar',
                                'role' => 'progress',
                                'style' => 'width: ' . $percent . '%;',
                                'aria-valuenow' => $current,
                                'aria-valuemin' => $min,
                                'aria-valuemax' => $max]),
                   ['class' => 'progress']);
    }
}
