<?php
namespace Cashflow\View\Helper;


class FormElement extends Base
{
    protected $_element;


    public function __invoke($element)
    {
        $this->_element = $element;

        return
            $this->_renderLabel()
            . $this->_renderInput()
            . $this->_renderDescription()
            . $this->_renderError();
    }


    protected function _renderInput()
    {
        $defaults = ['name' => $this->_element->getName(),
                     'id' => $this->_element->getId(),
                     'value' => $this->_element->getValue(),
                     'class' => 'form-control'];

        if ($this->_element->isRequired())
            $defaults['required'] = 'required';

        if ($this->_element->isDisabled())
            $defaults['disabled'] = 'disabled';

        return $this->_tag('input', null,
                           array_merge($defaults, $this->_inputAttribs()));
    }


    protected function _renderDescription()
    {
        return ($description = $this->_element->getDescription())
            ? $this->_tag('small',
                          $this->_view->fontAwesome('exclamation-circle') . ' ' . $description,
                          ['class' => 'form-text text-muted'])
            : '';
    }


    protected function _renderLabel()
    {
        return ($label = $this->_element->getLabel())
            ? $this->_tag('label', $label, ['for' => $this->_element->getId()])
            : '';
    }


    protected function _renderError()
    {
        return ($error = $this->_element->getError())
            ? $this->_tag('small',
                          $this->_view->fontAwesome('exclamation-triangle') . ' ' . $error,
                          ['class' => 'text-danger'])
            : '';
    }


    protected function _inputAttribs()
    {
        return [];
    }
}
