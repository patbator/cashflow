<?php
namespace Cashflow\View\Helper;


class FontAwesome extends Base
{
    public function __invoke($name)
    {
        return $this->_tag('i', '', ['class' => 'fas fa-' . $name]);
    }
}
