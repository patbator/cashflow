<?php
namespace Cashflow\View\Helper;


class Tag extends Base
{
    public function __invoke($name, $content, $attribs=[])
    {
        return '<' . $name . $this->_htmlAttribs($attribs)
            . (null !== $content
               ? (' >' . $content . '</' . $name . '>')
               : ' />');
    }


    protected function _htmlAttribs($attribs)
    {
        $html = [];
        foreach($attribs as $name => $value)
            $html = $this->_htmlAttrib($name, $value, $html);

        return $html
            ? (' ' . implode(' ', $html))
            : '';
    }


    protected function _htmlAttrib($name, $value, $html)
    {
        if (preg_match('@[ "\'/>=[:cntrl:]]@iu', $name))
            return $html;

        $html[] = $name . (null === $value ? '' : ('="' . $this->_view->escape($value) . '"'));
        return $html;
    }
}
