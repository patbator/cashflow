<?php
namespace Cashflow\View\Helper;


class FormSelect extends FormElement
{
    protected function _renderInput()
    {
        $defaults = ['name' => $this->_element->getName(),
                     'id' => $this->_element->getId(),
                     'value' => $this->_element->getValue(),
                     'class' => 'form-control'];

        if ($this->_element->isRequired())
            $defaults['required'] = 'required';

        if ($this->_element->isDisabled())
            $defaults['disabled'] = 'disabled';

        return $this->_tag('select', $this->_renderOptions(),
                           array_merge($defaults, $this->_inputAttribs()));
    }


    protected function _renderOptions()
    {
        $options = [];
        foreach($this->_element->getOptions() as $value => $label)
            $options[] = $this->_renderOption($value, $label);

        return implode("\n", $options);
    }


    protected function _renderOption($value, $label)
    {
        $attribs = ['value' => $value];
        if ($value ==$this->_element->getValue())
            $attribs['selected'] = 'selected';

        return $this->_tag('option', $label, $attribs);
    }
}
