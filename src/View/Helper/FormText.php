<?php
namespace Cashflow\View\Helper;


class FormText extends FormElement
{
    protected function _inputAttribs()
    {
        return ['type' => 'text'];
    }
}
