<?php
namespace Cashflow\View\Helper;


class Pagination extends Base
{
    protected $_current, $_total, $_url;

    public function __invoke($current, $total, $url)
    {
        $this->_current = $current;
        $this->_total = $total;
        $this->_url = $url;

        return $this->_tag('nav',
                           $this->_tag('ul',
                                       $this->_first()
                                       . $this->_previous()
                                       . $this->_current()
                                       . $this->_next()
                                       . $this->_last(),
                                       ['class' => 'pagination']));
    }


    protected function _first()
    {
        return $this->_item('step-backward', 0, $this->_current == 0);
    }


    protected function _previous()
    {
        return $this->_item('backward', $this->_current-1, $this->_current == 0);
    }


    protected function _current()
    {
        return $this->_pageItem(($this->_current+1) . ' / ' . $this->_total,
                                $this->_current,
                                false);
    }


    protected function _next()
    {
        return $this->_item('forward', $this->_current+1, $this->_current == $this->_total-1);
    }


    protected function _last()
    {
        return $this->_item('step-forward', $this->_total-1, $this->_current == $this->_total-1);
    }


    protected function _item($icon, $page, $disabled)
    {
        return $this->_pageItem($this->_view->fontAwesome($icon),
                                $page,
                                $disabled);
    }


    protected function _pageItem($content, $page, $disabled)
    {
        return $this
            ->_tag('li',
                   $this->_tag('a',
                               $content,
                               ['href' => $this->_url . '/' . $page,
                                'class' => 'page-link']),
                   ['class' => 'page-item' . ($disabled ? ' disabled' : '')]);
    }
}
