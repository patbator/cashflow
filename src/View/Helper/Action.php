<?php
namespace Cashflow\View\Helper;


class Action extends Base
{
    public function __invoke($label, $url, $icons, $btn_type, $attribs=[])
    {
        $attribs = array_merge(['href' => $url,
                                'title' => $label,
                                'class' => 'btn btn-sm btn-' . $btn_type],
                               $attribs);

        return $this->_tag('a', $this->_icons($icons), $attribs);
    }


    protected function _icons($icons)
    {
        if (!$icons)
            return '';

        if (!is_array($icons))
            $icons = [$icons];

        return implode(
            ' ',
            array_map(
                function($icon) {
                    return $this->_tag('i', '', ['class' => 'fas fa-' . $icon]);
                },
                $icons
            )
        );
    }
}
