<?php
namespace Cashflow\View\Helper;


class RenderTable extends Base
{
    protected
        $_headers = [],
        $_contents = [];

    public function __invoke($id, $description, $items)
    {
        $this->_headers = $this->_contents = [];

        $description->eachColumnsDo(function($col) {
            $col->acceptVisitor($this);
        });

        $this->_view->addDocumentReady('$("#' . $id . '").DataTable();');

        return $this->_tag('table', $this->_headers() . $this->_rows($items),
                           ['class' => 'table',
                            'id' => $id]);
    }


    protected function _headers()
    {
        return $this->_tag('thead',
                           $this->_tag('tr', implode($this->_headers)));
    }


    protected function _rows($items)
    {
        $rows = [];
        foreach($items as $item)
            $rows[] = $this->_row($item);

        return $this->_tag('tbody', implode($rows));
    }


    protected function _row($item) {
        $datas = [];
        foreach ($this->_contents as $content)
            $datas[] = $this->_tag('td', $content($item));

        return $this->_tag('tr', implode($datas));
    }


    public function visitLabel($label, $icon)
    {
        if ($icon)
            $label = $this->_tag('i', '', ['class' => 'fas fa-' . $icon]) . ' ' . $label;
        $this->_headers[] = $this->_tag('th', $label, ['scope' => 'col']);

        return $this;
    }


    public function visitContent($content)
    {
        $this->_contents[] = $content;
        return $this;
    }
}
