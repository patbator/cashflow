<?php
namespace Cashflow\View\Helper;


class FormDate extends FormElement
{
    protected function _inputAttribs()
    {
        return ['type' => 'date',
                'min' => $this->_element->getMin(),
                'max' => $this->_element->getMax()];
    }
}
