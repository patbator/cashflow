<?php
namespace Cashflow\View;

use \Patbator\Collection\Collection;


class Table
{
    protected $_columns;


    public function __construct()
    {
        $this->_columns = new Collection();
    }


    public function addColumn($label, $content, $icon=null)
    {
        $this->_columns->append(new TableColumn($label, $content, $icon));
        return $this;
    }


    public function eachColumnsDo($closure)
    {
        $this->_columns->eachDo($closure);
    }
}



class TableColumn
{
    protected $_label, $_label_icon, $_content;


    public function __construct($label, $content, $label_icon)
    {
        $this->_label = $label;
        $this->_content = $content;
        $this->_label_icon = $label_icon;
    }


    public function acceptVisitor($visitor)
    {
        $visitor
            ->visitLabel($this->_label, $this->_label_icon)
            ->visitContent($this->_content);
    }
}
