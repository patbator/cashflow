<?php
namespace Cashflow\View;

use \Patbator\Collection\Collection;


class Scripts extends Helper\Base
{
    protected
        $_document_ready,
        $_body_scripts,
        $_head_links,
        $_head_scripts;

    public function __construct($view)
    {
        parent::__construct($view);
        $this->_document_ready = new Collection();
        $this->_head_links = new Collection();
        $this->_body_scripts = new Collection();
        $this->_head_scripts = new Collection();
    }


    public function addDocumentReady($script)
    {
        $this->_document_ready->append($script);
        return $this;
    }


    public function addStyleSheet($href)
    {
        return $this->addHeadLink($href, 'stylesheet');
    }


    public function addHeadLink($href, $rel)
    {
        $this->_head_links->offsetSet(
            md5(json_encode([$href, $rel])),
            $this->_view->tag('link', null, ['href' => $href, 'rel' => $rel])
        );

        return $this;
    }


    public function addHeadScript($href)
    {
        $this->_head_scripts->offsetSet(
            md5($href),
            $this->_view->tag('script', '', ['src' => $href])
        );

        return $this;
    }


    public function addBodyScript($href)
    {
        $this->_body_scripts->offsetSet(
            md5($href),
            $this->_view->tag('script', '', ['src' => $href])
        );

        return $this;
    }


    public function headLinks()
    {
        return implode("\n", $this->_head_links->getArrayCopy());
    }


    public function headScripts()
    {
        return implode("\n", $this->_head_scripts->getArrayCopy());
    }


    public function bodyScripts()
    {
        $scripts = $this->_body_scripts->getArrayCopy();
        if ($ready = $this->_documentReady())
            $scripts[] = $this->_tag('script', $ready);

        return implode("\n", $scripts);
    }


    protected function _documentReady()
    {
        return $this->_document_ready->isEmpty()
            ? ''
            : sprintf('$(document).ready(function() {%s});',
                      implode($this->_document_ready->getArrayCopy()));
    }
}
