<?php
namespace Cashflow\View;

use \Slim\Views\PhpRenderer;


class Renderer extends PhpRenderer
{
    protected $_scripts;


    public function __call($name, $params)
    {
        if (method_exists($this->getScripts(), $name))
            return call_user_func_array([$this->getScripts(), $name], $params);

        $helper_class = 'Cashflow\\View\\Helper\\' . ucfirst($name);
        if (!class_exists($helper_class))
            throw new \RuntimeException('Call to unknown method ' . get_class() . '::' . $name);

        $helper = new $helper_class($this);
        return call_user_func_array($helper, $params);
    }


    public function escape($value)
    {
        return htmlspecialchars($value, ENT_QUOTES|ENT_SUBSTITUTE, 'UTF-8');
    }


    public function getScripts()
    {
        return $this->_scripts
            ? $this->_scripts
            : $this->_scripts = new Scripts($this);
    }
}
