<?php
namespace Cashflow;

use \Storm\Model\ModelAbstract;
use \Storm\Model\Loader;
use \Storm\Model\Collection;
use \Storm\Collection as BaseCollection;
use \Cashflow\Flow\Group;
use \Cashflow\Flow\Import\CaisseEpargneCsv;


class FlowLoader extends Loader
{
    public function groupedByMonth($flows)
    {
        $groups = [];
        $closure = function($flow) { return $flow->getMonth(); };
        foreach($flows as $flow) {
            $month = $closure($flow);
            if (!array_key_exists($month, $groups))
                $groups[$month] = new Group($month, $closure);

            $flow->acceptVisitor($groups[$month]);
        }

        return new BaseCollection($groups);
    }


    public function findAllGroupedByMonth()
    {
        return Flow::groupedByMonth(Flow::findAll());
    }


    public function ofMonth($month)
    {
        return Flow::findAllBy(['where' => 'strftime(\'%Y-%m\', value_date) = \'' . $month . '\'']);
    }


    public function ofLastMonth()
    {
        return $this->_ofLastNMonths(1);
    }


    public function ofLastQuarter()
    {
        return $this->_ofLastNMonths(3);
    }


    public function ofLastSemester()
    {
        return $this->_ofLastNMonths(6);
    }


    public function ofLastNinemester()
    {
        return $this->_ofLastNMonths(9);
    }


    public function ofLastYear()
    {
        return $this->_ofLastNMonths(12);
    }


    protected function _ofLastNMonths($count)
    {
        $group = new Group('', function() {});
        if (!$last = Flow::findFirstBy(['order' => ['value_date' => 'desc']]))
            return $group;

        $date = new \DateTime(substr($last->getValueDate(), 0, 7) . '-01');

        $months = (1 == $count)
            ? [$date->format('Y-m')]
            : $this->_reverseMonthsFrom($date, $count-1);

        (new BaseCollection($months))
            ->eachDo(function($month) use($group) {
                $this->_addFlowsOfMonthInto($month, $group);
            });

        return $group;
    }


    protected function _reverseMonthsFrom($date, $times_back)
    {
        $months = [$date->format('Y-m')];
        foreach(range(1, $times_back) as $i) {
            $date->modify('-1 month');
            $months[] = $date->format('Y-m');
        }

        return $months;
    }


    protected function _addFlowsOfMonthInto($month, $group)
    {
        $flows = Flow::ofMonth($month);
        array_map(
            function($flow) use($group) { $flow->acceptVisitor($group); },
            $flows
        );
    }


    public function dateRange()
    {
        $range = new DateRange();

        (new Collection(Flow::findAll()))->eachDo(function($flow) use($range) {
            if (!$flow_date = $flow->getValueDate())
                return;

            if (false === $flow_time = strtotime($flow_date))
                return;

            $range
                ->setFromIfBefore($flow_time)
                ->setToIfAfter($flow_time);
        });

        return $range;
    }
}



class Flow extends ModelAbstract
{
    const
        TYPE_UNKNOWN   = 'unknown',
        TYPE_CARD      = 'card',
        TYPE_TRANSFERT = 'transfert',
        TYPE_CHECK     = 'check',
        TYPE_LEVY      = 'levy',
        TYPE_CASH      = 'cash',
        TYPE_BANK      = 'bank',

        DIRECTION_IN   = 'in',
        DIRECTION_OUT  = 'out'
        ;

    protected static $_icons = [
        self::TYPE_UNKNOWN   => 'question-circle',
        self::TYPE_CARD      => 'credit-card',
        self::TYPE_TRANSFERT => 'paper-plane',
        self::TYPE_CHECK     => 'money-check',
        self::TYPE_LEVY      => 'sync',
        self::TYPE_CASH      => 'money-bill-alt',
        self::TYPE_BANK      => 'syringe'
    ];

    protected
        $_table_name = 'flow',
        $_loader_class = FlowLoader::class,
        $_default_attribute_values = [
            'value_date' => '',
            'type' => self::TYPE_UNKNOWN,
            'in' => 0.0,
            'out' => 0.0,
            'details' => '',
            'raw' => '',
            'source_id' => '',
        ],
        $_belongs_to = ['subject' => ['model' => Subject::class]],
        $_has_many = ['tags' => ['through' => 'subject']],
        $_raw_model;


    public static function knownTypes()
    {
        return [
            static::TYPE_UNKNOWN   => 'Inconnu',
            static::TYPE_CARD      => 'Carte bancaire',
            static::TYPE_CHECK     => 'Chèque',
            static::TYPE_BANK      => 'Frais bancaires',
            static::TYPE_LEVY      => 'Prélèvement',
            static::TYPE_CASH      => 'Retrait d\'espèces',
            static::TYPE_TRANSFERT => 'Virement',
        ];
    }


    public static function knownDirections()
    {
        return [static::DIRECTION_IN  => 'Entrée',
                static::DIRECTION_OUT => 'Sortie'];
    }


    public function getTypeLabel()
    {
        return static::knownTypes()[$this->getType()];
    }


    public function getSubjectLabel()
    {
        return $this->hasSubject()
            ? $this->getSubject()->getLabel()
            : 'Unknown';
    }


    public function getMonth()
    {
        return substr($this->getValueDate(), 0, 7);
    }


    public function acceptVisitor($visitor)
    {
        $visitor->visitAmounts($this->getIn(), $this->getOut());
    }


    public function hasTag($label)
    {
        return $this->getTags()->detect(function($tag) use($label) {
            return $label === $tag->getLabel();
        });
    }


    public function getIcon()
    {
        return static::$_icons[$this->getType()];
    }


    public function getDirection()
    {
        return 0 < $this->getIn() ? static::DIRECTION_IN : static::DIRECTION_OUT;
    }


    public function isType($type)
    {
        return $this->getType() === $type;
    }


    public function getFormattedIn()
    {
        return $this->_getFormattedAmount($this->getIn());
    }


    public function getFormattedOut()
    {
        return $this->_getFormattedAmount($this->getOut());
    }


    protected function _getFormattedAmount($amount)
    {
        return sprintf('%0.2f', $amount);
    }


    public function isSubjectModified()
    {
        return $this->hasSubject()
            && $this->getRawSubject()->getLabel() != $this->getSubject()->getLabel();
    }


    public function isValueDateModified()
    {
        return $this->hasValueDate()
            && $this->getRawValueDate() != $this->getValueDate();
    }


    public function getRawSubject()
    {
        return $this->getRawModel()->getSubject();
    }


    public function getRawValueDate()
    {
        return $this->getRawModel()->getValueDate();
    }


    public function getRawModel()
    {
        if (!$this->_raw_model)
            $this->_raw_model = (new CaisseEpargneCsv(''))
                ->modelFrom(json_decode($this->getRaw(), true));

        return $this->_raw_model;
    }


    public function form()
    {
        return (new Form)
            ->withErrors($this->getErrors())
            ->addDisabledSelect('type', ['label' => 'Type',
                                         'value' => $this->getType(),
                                         'options' => static::knownTypes()])

            ->addRequiredEditableSelect('subject',
                                        ['label' => 'Sujet',
                                         'value' => $this->getSubjectId(),
                                         'options' => Subject::asFormOptions(),
                                         'description' => $this->isSubjectModified() ? ('Valeur originale : ' . $this->getRawSubject()->getLabel()) : ''])

            ->addRequiredDate('value_date',
                              ['label' => 'Date de valeur',
                               'value' => $this->getValueDate(),
                               'description' => $this->isValueDateModified() ? ('Valeur originale : ' . $this->getRawValueDate()) : ''])

            ->addDisabledText('in', ['label' => 'Entrée', 'value' => $this->getFormattedIn()])

            ->addDisabledText('out', ['label' => 'Sortie', 'value' => $this->getFormattedOut()])

            ->addDisabledText('details', ['label' => 'Détails', 'value' => $this->getDetails()])
            ;
    }
}
