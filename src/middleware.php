<?php
$container = $app->getContainer();

$app->add(new \Cashflow\Middleware\Layout($container['renderer'], $container['router']));
$app->add(new \Cashflow\Middleware\Db($container->get('settings')['db']['path']));
