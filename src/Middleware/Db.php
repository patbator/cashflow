<?php
namespace Cashflow\Middleware;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Storm\Persistence\Connections;
use \Storm\Persistence\Configuration;
use \Storm\Persistence\Sqlite\Connection;
use \Storm\Persistence\SqlStrategy;
use \Storm\Model\Loader;


class Db
{
    protected $_path;

    public function __construct($path)
    {
        $this->_path = $path;
    }


    public function __invoke(Request $request, Response $response, callable $next)
    {
        Connections::getInstance()
            ->setDefault(new Connection(new Configuration(['database' => $this->_path])));
        Loader::defaultTo(SqlStrategy::class);

        return $next($request, $response);
    }
}
