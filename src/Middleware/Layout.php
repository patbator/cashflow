<?php
namespace Cashflow\Middleware;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Slim\Http\Body;


class Layout
{
    protected
        $_renderer,
        $_router;


    public function __construct($renderer, $router)
    {
        $this->_renderer = $renderer;
        $this->_router = $router;
    }


    public function __invoke(Request $request, Response $response, callable $next)
    {
        $response = $next($request, $response);
        if ('application/json;charset=utf-8' === $response->getHeaderLine('Content-Type'))
            return $response;

        $body = $response->getBody();
        $body->rewind();
        $content = $body->getContents();
        $response = $response->withBody(new Body(fopen('php://temp', 'r+')));

        return $this->_renderer->render($response, 'layout.phtml',
                                        ['content' => $content,
                                         'router' => $this->_router]);
    }
}
