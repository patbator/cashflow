<?php
namespace Cashflow;

use \Patbator\Collection\Collection;


class Form extends Collection
{
    protected $_errors = [];


    public function __construct()
    {
        parent::__construct();
        $this->setFlags(static::ARRAY_AS_PROPS);
    }


    public function __call($name, $params=[])
    {
        if ('add' == substr($name, 0, 3))
            return $this->_addElement(substr($name, 3), $params[0], $params[1]);

        throw new \RuntimeException('Call to unkown method ' . static::class . '::' . $name);
    }


    public function withErrors($errors)
    {
        $this->_errors = $errors;
        return $this;
    }


    public function values()
    {
        $values = [];
        $this->eachDo(function($element) use(&$values) {
            $values[$element->getName()] = $element->getValue();
        });

        return $values;
    }


    protected function _addElement($type, $name, $options)
    {
        if ('required' == substr(strtolower($type), 0, 8)) {
            $options['required'] = true;
            $type = substr($type, 8);
        }

        if ('disabled' == substr(strtolower($type), 0, 8)) {
            $options['disabled'] = true;
            $type = substr($type, 8);
        }

        $this->offsetSet($name, $element = new FormElement(strtolower($type), $name, $options));

        if (array_key_exists($name, $this->_errors))
            $element->addError($this->_errors[$name]);

        return $this;
    }
}



class FormElement
{
    protected
        $_type,
        $_name,
        $_options,
        $_error;

    public function __construct($type, $name, $options)
    {
        $this->_type = $type;
        $this->_name = $name;
        $this->_options = $options;
    }


    public function renderOn($view)
    {
        $helper = 'form' . ucfirst($this->_type);
        return $view->$helper($this);
    }


    public function getLabel()
    {
        return $this->_getOption('label');
    }


    public function getId()
    {
        return $this->getName();
    }


    public function getName()
    {
        return $this->_name;
    }


    public function getValue()
    {
        return $this->_getOption('value');
    }


    public function getMin()
    {
        return $this->_getOption('min');
    }


    public function getMax()
    {
        return $this->_getOption('max');
    }


    public function getCols()
    {
        return $this->_getOption('cols', 80);
    }


    public function getRows()
    {
        return $this->_getOption('rows', 5);
    }


    public function getDescription()
    {
        return $this->_getOption('description');
    }


    public function isRequired()
    {
        return true === $this->_getOption('required', false);
    }


    public function isDisabled()
    {
        return true === $this->_getOption('disabled', false);
    }


    public function addError($message)
    {
        $this->_error = $message;
        return $this;
    }


    public function hasError() {
        return $this->_error;
    }


    public function getError()
    {
        return $this->_error;
    }


    protected function _getOption($name, $default='')
    {
        return isset($this->_options[$name])
            ? $this->_options[$name]
            : $default;
    }


    public function setValue($value)
    {
        $this->_options['value'] = $value;
        return $this;
    }


    public function getOptions()
    {
        return $this->_getOption('options', []);
    }
}
