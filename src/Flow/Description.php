<?php
namespace Cashflow\Flow;

use \Cashflow\Repository;
use \Cashflow\Model;


class Description extends Model
{
    protected static
        $_repository_class = DescriptionRepository::class,
        $_primary = 'target',
        $_definition = ['target', 'content'];


    public function isSameAs($other)
    {
        return $this->_target === $other->_target;
    }


    public function isTargetting($flow)
    {
        return $this->_target === $flow->getId();
    }
}



class DescriptionRepository extends Repository
{
    protected $_label = 'Descriptions';

    public function findOrCreateFor($flow)
    {
        return ($model = Description::find($flow))
            ? $model
            : new Description(['target' => $flow->getId()]);
    }


    public function find($flow)
    {
        return $this->all()->detect(function($item) use($flow) {
            return $item->isTargetting($flow);
        });
    }


    protected function _path()
    {
        return __DIR__ . '/../../datas/descriptions.json';
    }
}
