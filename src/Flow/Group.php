<?php
namespace Cashflow\Flow;

use \Cashflow\Tag;


class Group
{
    protected
        $_label = '',
        $_count = 0,
        $_in = 0,
        $_out = 0,
        $_filter_closure;

    public function __construct($label, $filter_closure)
    {
        $this->_label = $label;
        $this->_filter_closure = $filter_closure;
    }


    public function visitAmounts($in, $out)
    {
        $this->_count++;
        $this->_in += $in;
        $this->_out += $out;
    }


    public function getLabel()
    {
        return $this->_label;
    }


    public function getCount()
    {
        return $this->_count;
    }


    public function getIn()
    {
        return (int)$this->_in;
    }


    public function isIn()
    {
        return 0 < $this->_in;
    }


    public function getOut()
    {
        return (int)$this->_out;
    }


    public function isOut()
    {
        return 0 < $this->_out;
    }


    public function getBalance()
    {
        return (int)($this->_in - $this->_out);
    }


    public function hasTag($tag)
    {
        return $tag
            ? $tag->hasTarget($this->_label)
            : false;
    }


    public function isClassified()
    {
        return !Tag::forLabel($this->_label)->isEmpty();
    }


    public function getFlows()
    {
        return \Cashflow\Flow::all()->select(function($item) {
            return call_user_func($this->_filter_closure, $item) === $this->_label;
        });
    }
}
