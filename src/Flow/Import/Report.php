<?php
namespace Cashflow\Flow\Import;


class Report
{
    protected
        $_valid = 0,
        $_invalid_column_count = 0,
        $_already_imported = 0;

    public function addValid()
    {
        $this->_valid++;
    }


    public function getValid()
    {
        return $this->_valid;
    }


    public function addInvalidColumnCount()
    {
        $this->_invalid_column_count++;
    }


    public function addAlreadyImported()
    {
        $this->_already_imported++;
    }


    public function getAlreadyImported()
    {
        return $this->_already_imported;
    }
}
