<?php
namespace Cashflow\Flow\Import;

use \Cashflow\Flow;
use \Cashflow\Subject;


class CaisseEpargneCsv
{
    protected
        $_file,
        $_flow;

    public function __construct($file_path)
    {
        $this->_file = new \SplFileInfo($file_path);
    }


    public function run()
    {
        $report = new Report();
        if (!$this->_file->isFile() || !$this->_file->isReadable())
            return $report->beUnreadableFile();

        $file = $this->_file->openFile();
        while (!$file->eof()) {
            if (!$data = $file->fgetcsv(';')) {
                $report->addSkip();
                continue;
            }

            $this->_importOne($data, $report);
        }

        return $report;
    }


    protected function _importOne($datas, $report)
    {
        if (6 > count($datas)) {
            $report->addInvalidColumnCount();
            return;
        }

        $id = $datas[1];
        if (Flow::findFirstby(['source_id' => $datas[1]])) {
            $report->addAlreadyImported();
            return;
        }

        $report->addValid();
        $this->modelFrom($datas)->save();
    }


    public function modelFrom($datas)
    {
        return Flow::newInstance($this->datasToAttributes($datas));
    }


    public function datasToAttributes($datas)
    {
        $attributes = [
            'raw' => json_encode($datas),
            'source_id' => $datas[1],
            'details' => $datas[2],
            'in' => 0.0,
            'out' => 0.0,
            'type' => Flow::TYPE_UNKNOWN,
            'subject' => $this->_subjectFrom('Unknown'),
        ];

        $date = substr($datas[1], 8, 8);
        $attributes['value_date'] = implode('-', [substr($date, 0, 4),
                                                  substr($date, 4, 2),
                                                  substr($date, 6, 2)]);

        '' != $datas[3]
            ? ($attributes['out'] = (float) str_replace([',', '-'], ['.', ''], $datas[3]))
            : ($attributes['in'] = (float) str_replace(',', '.', $datas[4]));

        $attributes = $this->_typeFrom($attributes);

        return $attributes;
    }


    protected function _typeFrom($attributes)
    {
        $subject = $attributes['details'];

        $known = [
            'CB' => '_typeCard',
            'VIR' => '_typeTransfert',
            'CHEQUE' => '_typeCheck',
            'PRLV' => '_typeLevy',
            'RETRAIT' => '_typeCash',
        ];

        $subject_parts = explode(' ', $subject);
        if (in_array($subject_parts[0], array_keys($known)))
            return call_user_func([$this, $known[$subject_parts[0]]], $subject, $attributes);

        if ('*' == substr($subject, 0, 1))
            return $this->_typeBank($subject, $attributes);

        return $attributes;
    }


    protected function _typeCard($subject, $attributes)
    {
        // "CB [SUBJECT] FACT [DDMMYY]"
        $parts = explode(' ', $subject);
        $attributes['type'] = Flow::TYPE_CARD;
        $attributes['subject'] = $this->_subjectFrom(
            implode(' ',
                    array_slice($parts, 1, count($parts) - 3))
        );

        return $attributes;
    }


    protected function _typeTransfert($subject, $attributes)
    {
        // "VIR SEPA [SUBJECT]"
        $parts = explode(' ', $subject);
        $attributes['type'] = Flow::TYPE_TRANSFERT;
        $attributes['subject'] = $this->_subjectFrom(implode(' ', array_slice($parts, 2)));

        return $attributes;
    }


    protected function _typeCheck($subject, $attributes)
    {
        // "CHEQUE N [SUBJECT]"
        $parts = explode(' ', $subject);
        $attributes['type'] = Flow::TYPE_CHECK;
        $attributes['subject'] = $this->_subjectFrom(end($parts));

        return $attributes;
    }


    protected function _typeLevy($subject, $attributes)
    {
        // "PRLV [SUBJECT]"
        $parts = explode(' ', $subject);
        $attributes['type'] = Flow::TYPE_LEVY;
        $attributes['subject'] = $this->_subjectFrom(implode(' ', array_slice($parts, 1)));

        return $attributes;
    }


    protected function _typeCash($subject, $attributes)
    {
        // "RETRAIT DAB [SUBJECT]"
        $parts = explode(' ', $subject);
        $attributes['type'] = Flow::TYPE_CASH;
        $attributes['subject'] = $this->_subjectFrom(implode(' ', array_slice($parts, 2)));

        return $attributes;
    }


    protected function _typeBank($subject, $attributes)
    {
        // "*[SUBJECT]"
        $attributes['type'] = Flow::TYPE_BANK;
        $attributes['subject'] = $this->_subjectFrom(substr($subject, 1));

        return $attributes;
    }


    protected function _subjectFrom($label)
    {
        return Subject::findOrCreateByLabel(trim($label));
    }
}
