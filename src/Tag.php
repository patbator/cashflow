<?php
namespace Cashflow;

use \Storm\Model\ModelAbstract;
use \Storm\Model\Loader;


class TagLoader extends Loader
{
    public function asFormOptions()
    {
        $tags = [];
        foreach(Tag::findAll() as $tag)
            $tags[$tag->getId()] = $tag->getLabel();

        return $tags;
    }
}



class Tag extends ModelAbstract
{
    protected
        $_table_name = 'tag',
        $_loader_class = TagLoader::class,
        $_default_attribute_values = ['label' => ''],
        $_has_many = [
            'subjecttags' => ['model' => SubjectTag::class,
                              'role' => 'tag',
                              'unique' => true,
                              'dependents' => 'delete'],

            'subjects' => ['through' => 'subjecttags'],
        ];


    public function validate()
    {
        $this->checkAttribute('label', '' != $this->getLabel(),
                              'Le libellé ne peut pas être vide');
    }


    public function form()
    {
        return (new Form)
            ->withErrors($this->getErrors())
            ->addRequiredText('label', ['label' => 'Libellé',
                                        'value' => $this->getLabel()])
            ;
    }
}
