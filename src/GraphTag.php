<?php
namespace Cashflow;

use \Storm\Model\ModelAbstract;


class GraphTag extends ModelAbstract
{
    protected
        $_table_name = 'graph_tag',
        $_belongs_to = [
            'graph' => ['model' => Graph::class],
            'tag' => ['model' => Tag::class],
        ];
}
