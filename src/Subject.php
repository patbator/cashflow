<?php
namespace Cashflow;

use \Storm\Model\ModelAbstract;
use \Storm\Model\Collection;
use \Storm\Model\Loader;


class SubjectLoader extends Loader
{
    public function findOrCreateByGroup($group)
    {
        return Subject::findOrCreateByLabel($group->getLabel())->setGroup($group);
    }


    public function findOrCreateByLabel($label)
    {
        $attribs = ['label' => $label];
        if (!$subject = Subject::findFirstBy($attribs))
            $subject = Subject::newInstance($attribs);

        return $subject;
    }


    public function asFormOptions()
    {
        $models = [];
        foreach(Subject::findAllBy(['order' => 'label']) as $model)
            $models[$model->getId()] = $model->getLabel();

        return $models;
    }
}



class Subject extends ModelAbstract
{
    protected
        $_table_name = 'subject',
        $_loader_class = SubjectLoader::class,
        $_default_attribute_values = ['label' => '',
                                      'description' => ''],
        $_has_many = [
            'subjecttags' => ['model' => SubjectTag::class,
                              'role' => 'subject',
                              'unique' => true,
                              'dependents' => 'delete'],

            'tags' => ['through' => 'subjecttags',
                       'unique' => true],

            'flows' => ['model' => Flow::class,
                        'role' => 'subject'],
        ];


    public function getIn()
    {
        return (new Collection($this->getFlows()))
            ->injectInto(
                0,
                function($current, $flow) { return $current + $flow->getIn(); }
            );
    }


    public function getOut()
    {
        return (new Collection($this->getFlows()))
            ->injectInto(
                0,
                function($current, $flow) { return $current + $flow->getOut(); }
            );
    }


    public function isClassified()
    {
        return $this->hasSubjecttags();
    }


    public function isType($type)
    {
        return (new Collection($this->getFlows()))
            ->detect(function($flow) use($type) {
                return $flow->isType($type);
            });
    }


    public function form()
    {
        $current_tags = (new Collection($this->getTags()))
            ->collect('id')
            ->getArrayCopy();

        return (new Form)
            ->withErrors($this->getErrors())
            ->addCheckboxes('tags', ['label' => 'Tags',
                                     'value' => $current_tags,
                                     'options' => Tag::asFormOptions()])
            ->addTextarea('description', ['value' => $this->getDescription()])
            ;

    }
}
