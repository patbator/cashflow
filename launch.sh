#!/bin/bash

trap "exit" INT TERM ERR
trap "kill 0" EXIT

cd `dirname "$0"`
php -S 0.0.0.0:8080 -t public/ public/router.php &
firefox http://localhost:8080/

wait
